# Cucumber Dagger

An example of how to use the [Dagger Dependency Injection framework](https://dagger.dev/) with Cucumber.

This is a repo to go side-by-side with [_Using Dagger for Dependency Injection with Cucumber Tests_](https://www.jvt.me/posts/2021/12/30/cucumber-dagger-dependency-injection/).

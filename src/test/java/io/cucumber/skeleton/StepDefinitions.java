package io.cucumber.skeleton;

import io.cucumber.java.en.Given;
import io.cucumber.skeleton.config.Config;
import io.cucumber.skeleton.config.DaggerConfig;

public class StepDefinitions {

    private final Belly belly;

    public StepDefinitions() {
        this(DaggerConfig.create());
    }

    StepDefinitions(Config config) {
        this.belly = config.belly();
    }

    @Given("I have {int} cukes in my belly")
    public void I_have_cukes_in_my_belly(int cukes) {
        belly.eat(cukes);
    }
}

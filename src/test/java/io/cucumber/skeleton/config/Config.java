package io.cucumber.skeleton.config;

import dagger.Component;
import io.cucumber.skeleton.Belly;

import javax.inject.Singleton;

@Singleton
@Component(modules = {ConfigModule.class})
public interface Config {
    Belly belly();
}

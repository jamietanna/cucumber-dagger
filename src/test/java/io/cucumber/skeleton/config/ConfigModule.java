package io.cucumber.skeleton.config;

import dagger.Module;
import dagger.Provides;
import io.cucumber.skeleton.Belly;

import javax.inject.Singleton;

@Module
public class ConfigModule {

    private ConfigModule() {
        throw new UnsupportedOperationException("Utility class");
    }

    @Provides
    @Singleton
    public static Belly belly() {
        return new Belly();
    }
}
